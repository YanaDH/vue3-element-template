export type userType = {
  token: string
  userName: string
  userId: number
}
